<?php

declare(strict_types=1);

namespace spec\Paneric\DTOHydrator;

use Paneric\DTOHydrator\AttributesPresenterInterface;
use Paneric\DTOHydrator\DTOHydrator;
use PhpSpec\ObjectBehavior;

class DTOHydratorSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(DTOHydrator::class);
        $this->shouldBeAnInstanceOf(DTOHydrator::class);
    }

    public function it_hydrates()
    {
        $propertiesValues = [
            'id' => 4,
            'name' => 'Name',
            'updated_at_date' => 'date_time_updated',
            'created_at_' => 'date_time_created'
        ];

        $object = new TestObject();

        $this->hydrate($object, $propertiesValues)->shouldReturn($object);
        $this->hydrate($object, $propertiesValues)->getId()->shouldReturn(4);
        $this->hydrate($object, $propertiesValues)->getName()->shouldReturn('Name');
        $this->hydrate($object, $propertiesValues)->getUpdatedAtDate()->shouldReturn('date_time_updated');
    }

    public function it_extracts()
    {
        $object = new TestObject();
        $object->setId(4);
        $object->setName('Name');
        $object->setUpdatedAtDate('date_time_updated');

        $this->extract($object)->shouldHaveCount(3);

        $this->extract($object)->shouldHaveKey('id');
        $this->extract($object)->shouldHaveKey('name');
        $this->extract($object)->shouldHaveKey('updated_at_date');

        $this->extract($object)->shouldHaveValue(4);
        $this->extract($object)->shouldHaveValue('Name');
        $this->extract($object)->shouldHaveValue('date_time_updated');
    }

    public function getMatchers(): array
    {
        return [
            'haveKey' => function ($subject, $key) {
                return array_key_exists($key, $subject);
            },
            'haveValue' => function ($subject, $value) {
                return in_array($value, $subject);
            },
        ];
    }
}

class TestObject implements AttributesPresenterInterface
{
    private $id;
    private $name;
    private $associations;
    private $createdAt;
    private $updatedAtDate;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getUpdatedAtDate()
    {
        return $this->updatedAtDate;
    }

    public function setUpdatedAtDate($updatedAtDate): void
    {
        $this->updatedAtDate = $updatedAtDate;
    }

    public function getAttributesNames(): array
    {
        return array_keys(get_object_vars($this));
    }
}
