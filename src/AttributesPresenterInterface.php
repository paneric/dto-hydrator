<?php

declare(strict_types=1);

namespace Paneric\DTOHydrator;

interface AttributesPresenterInterface
{
    public function getAttributesNames(): array;
}
