<?php

declare(strict_types=1);

namespace Paneric\DTOHydrator;

class DTOHydrator
{
    public function hydrate(object $object, array $propertiesValues): object
    {
        foreach ($propertiesValues as $arrayKey => $value) {

            $methodName = $this->setSetMethodName($object, $arrayKey);

            if ($methodName !== null) {

                $object->$methodName($value);
            }
        }

        return $object;
    }

    public function extract(object $object): array
    {
        $array = [];

        $objectKeys = $object->getAttributesNames();

        foreach ($objectKeys as $value) {

            $methodName = $this->setGetMethodName($object, $value);

            if ($methodName !== null) {

                $arrayKey = strtolower(preg_replace('/(?<!^)[A-Z]/u', '_$0', $value));

                $array[$arrayKey] = $object->$methodName();
            }
        }

        return $array;
    }

    private function setSetMethodName(object $object, string $arrayKey): ?string
    {
        $keyParts = explode('_', $arrayKey);

        $methodName = 'set';

        foreach ($keyParts as $part) {
            $methodName .= ucfirst($part);
        }

        if (method_exists($object, $methodName)) {
            return $methodName;
        }

        return null;
    }

    private function setGetMethodName(object $object, string $objectKey): ?string
    {
        $methodName = 'get' . ucfirst($objectKey);

        if (method_exists($object, $methodName)) {
            return $methodName;
        }

        return null;
    }
}
